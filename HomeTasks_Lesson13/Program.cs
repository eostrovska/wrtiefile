﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace HomeTasks_Lesson13
{
    class Program
    {
        private static Random random = new Random();

        private static string RandomString()
        {
            int size = random.Next(5, 12);
            StringBuilder builder = new StringBuilder(size);

            for (int i = 0; i < size; i++)
                builder.Append((char)random.Next(0x41, 0x5A));
            return builder.ToString();
        }

        public static int CountMailAddress(string pattern, StreamReader sr)
        {
            int amount = new Regex(pattern).Matches(sr.ReadToEnd()).Count;
            return amount;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Task 1: Create a text file and write to it millions of mailboxes gmail, yandex, ukr.net." +
                              "Then read the data from the file and using the regulars to count how many mailboxes you have gmail, yandex, etc.");
            Console.WriteLine(new string('-', 70));

            var fileName = "MailStore.txt";
            var filePath = Directory.GetCurrentDirectory() + fileName;

            var file = new FileInfo(filePath);
            Random rand = new Random();
           
            StreamWriter writer = file.CreateText();
            writer.WriteLine("Mail Store");
           
            for (int i = 0; i < 4500000; i++)
            {
                writer.WriteLine(RandomString() + "@gmail.com");
                writer.WriteLine(RandomString() + "@yandex.com");
                writer.WriteLine(RandomString() + "@ukr.com");
            }

            writer.Close();

            string patternGmail = @"gmail";
            string patternYandex = @"yandex";
            string patternUkr = @"ukr";

            StreamReader sr = new StreamReader(filePath);
            //int resultGmail = CountMailAddress(patternGmail, sr);
            //int resultYandex = CountMailAddress(patternYandex, sr);
            int resultUkr = CountMailAddress(patternUkr, sr);

            //Console.WriteLine("Mail Store has {0} gmail address", resultGmail);
            //Console.WriteLine("Mail Store has {0} yandex address", resultYandex);
            Console.WriteLine("Mail Store has {0} ukr address", resultUkr);

            sr.Close();

            Console.ReadKey();
        }
    }
}
